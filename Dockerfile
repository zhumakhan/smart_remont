FROM python:3.8.0-alpine3.10

ENV PYTHONUNBUFFERED=1 COLUMNS=200 TZ=Asia/Almaty

COPY ./src /src
WORKDIR /src
# Add alpine mirrors from KZ
RUN sed -i 's/dl-cdn.alpinelinux.org/mirror.neolabs.kz/g' /etc/apk/repositories
# Set timezone
RUN    apk add tzdata && \
        ln -fs /usr/share/zoneinfo/Asia/Almaty /etc/localtime && \
        echo "Asia/Almaty" > /etc/timezone && apk del tzdata && \
# Add system dependencies
        apk update && \
        apk add bash gcc musl-dev libffi-dev libressl-dev jpeg-dev zlib-dev curl-dev linux-headers && \

        pip install -U -r requirements.txt