from django.contrib import admin

# Register your models here.
from .models import *

class ImageInline(admin.TabularInline):
	model=Image
	extra=0

@admin.register(Feature,Category)
class BasicAdmin(admin.ModelAdmin):
	pass

@admin.register(Product) 
class ProductAdmin(admin.ModelAdmin):
	inlines=(ImageInline,)
