import django_filters
from .models import Product,Feature


class ProductFilter(django_filters.rest_framework.FilterSet):
	feature=django_filters.CharFilter(method='feature_filter')
	category=django_filters.CharFilter('category__name')
	
	class Meta:
		model = Product
		fields = ['feature','category']
	
	def feature_filter(self,qs,name,values):
		values=values.split(',')
		return qs.filter(category__feature__name__in=values).distinct()

