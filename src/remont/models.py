from django.db import models

# Create your models here.

class Feature(models.Model):
	name=models.CharField(max_length=255,unique=True,db_index=True,verbose_name='Название')

	def __str__(self):
		return '{}'.format(self.name)

class Category(models.Model):
	name=models.CharField(max_length=255,unique=True,db_index=True,verbose_name='Название')
	feature=models.ManyToManyField(Feature)
	
	def __str__(self):
		return '{}'.format(self.name)



class Product(models.Model):
	name=models.CharField(max_length=255,verbose_name='Название')
	price=models.FloatField(verbose_name='Цена')
	brand=models.CharField(max_length=255,verbose_name='Бренд')
	category=models.ForeignKey(Category,on_delete=models.CASCADE)

	def __str__(self):
		return '{}'.format(self.name)



class Image(models.Model):
	product=models.ForeignKey(Product,on_delete=models.CASCADE,related_name='images')
	name=models.CharField(max_length=255,verbose_name='Название')
	file=models.ImageField('products')