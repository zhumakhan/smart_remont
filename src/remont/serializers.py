from rest_framework import serializers

from .models import *

class FeatureSerializer(serializers.ModelSerializer):
	class Meta:
		model=Feature
		fields='__all__'


class CategorySerializer(serializers.ModelSerializer):
	feature=FeatureSerializer(many=True)
	class Meta:
		model=Category
		fields='__all__'


class ImageSerializer(serializers.ModelSerializer):
	class Meta:
		model=Image
		fields='__all__'

class ProductSerializer(serializers.ModelSerializer):
	images = ImageSerializer(many=True,read_only=True)
	class Meta:
		model=Product
		exclude=('category',)