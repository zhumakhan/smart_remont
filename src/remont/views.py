from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import DjangoFilterBackend


from .serializers import *
from .models import *
from .filters import *



class CategoryView(viewsets.ReadOnlyModelViewSet):
	serializer_class = CategorySerializer
	queryset = Category.objects.all()
	permission_classes=(AllowAny,)

class ProductView(viewsets.ModelViewSet):
	serializer_class=ProductSerializer
	queryset=Product.objects.prefetch_related('category','category__feature')
	permission_classes=(AllowAny,)
	# filter_backends=(DjangoFilterBackend,)
	# filterset_fields=('')
	filterset_class=ProductFilter