asgiref==3.3.1
Django==3.1.4
pytz==2020.4
sqlparse==0.4.1
django-environ==0.4.5
djangorestframework==3.11.0
django-cors-headers==3.2.0
django-filter-2.4.0
django-coreapi==2.3.0
Pillow==8.0.1